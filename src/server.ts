import express from "express"
import AppRouter from "./router"
import AppException from "./exception/AppException"
import HttpStatus from "./defines/HttpStatus"

class AppServer {
  private app: express.Application
  private host: string
  private port: number
  private errorFunc: (error: any, req: express.Request, res: express.Response, next: express.NextFunction) => any

  constructor(port: number = 8080, host: string = "0.0.0.0") {
    this.app = express()
    this.port = port
    this.host = host
    this.errorFunc = this.selfErrorHandle
  }

  public init(callback: (app: express.Application) => void): void {
    callback(this.app)
  }

  public start(callback?: (server: AppServer) => void): void {
    this.app.use((_req, _res, next) => {
      next(new AppException("Page not found", "PAGE_NOT_FOUND", HttpStatus.NOT_FOUND))
    })

    this.app.use(this.errorFunc)

    this.app.listen(this.port, this.host, (err) => {
      if (err) throw err

      if (callback) callback(this)

      console.log(`Server is running at: ${this.host}:${this.port}`)
    })
  }

  public addMiddleware(callback: express.RequestHandler | express.RequestHandler[]): AppServer {
    this.app.use(callback)
    return this
  }

  public addRoute(path: string, callback: express.RequestHandler | express.RequestHandler[] | AppRouter): AppServer {
    if (callback instanceof AppRouter) {
      this.app.use(path, callback.getRouter())
    } else {
      this.app.use(path, callback)
    }

    return this
  }

  public addServeStatic(path: string): AppServer {
    this.app.use(express.static(path))
    return this
  }

  public handleError(
    errorCallback: (error: any, req: express.Request, res: express.Response, next: express.NextFunction) => any
  ): AppServer {
    this.errorFunc = errorCallback
    return this
  }

  private selfErrorHandle(error: any, _req: express.Request, res: express.Response, _next: express.NextFunction): any {
    if (error instanceof AppException) {
      return res.status(error.statusCode).json(error.toJson())
    }

    return res.status(500).send({
      message: "INTERNAL_SERVER_ERROR",
      debuginfo: error.stack,
      debugmessage: error.message
    })
  }
}

export default AppServer
