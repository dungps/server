import express from "express"
import HttpMethods from "../defines/HttpMethods"
import HttpStatus from "../defines/HttpStatus"

interface CORSConfig {
  allowOrigin: string
  allowHeaders: string
  allowCredentials: string
  allowMethods: string
  maxAge: string
  others?: { [index: string]: string }
}

const defaultConfig: CORSConfig = {
  allowOrigin: "*",
  allowHeaders: "Content-Type, Authentication, Authorization",
  allowCredentials: "true",
  allowMethods: "POST, GET, OPTIONS, DELETE, HEAD, PATCH, PUT",
  maxAge: "3600"
}

export default (config: CORSConfig = defaultConfig) => {
  return (req: express.Request, res: express.Response, next: express.NextFunction) => {
    res.header("Access-Control-Allow-Origin", config.allowOrigin)
    res.header("Access-Control-Allow-Headers", config.allowHeaders)
    res.header("Access-Control-Allow-Credentials", config.allowCredentials)
    res.header("Access-Control-Allow-Methods", config.allowMethods)
    res.header("Access-Control-Max-Age", config.maxAge)

    if (config.others !== undefined) {
      const headers = config.others
      const keys = Object.keys(config.others)

      if (keys.length > 0) {
        keys.forEach((key: any) => {
          res.header(key, headers[key])
        })
      }
    }

    if (req.method === HttpMethods.OPTIONS) {
      return res.sendStatus(HttpStatus.OK)
    }

    return next()
  }
}
