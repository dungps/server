class BaseException extends Error {
  public statusCode: any
  public errorCode: string

  constructor(message: string, errorCode: string, statusCode: any) {
    super(message)
    this.errorCode = errorCode
    this.statusCode = statusCode
  }

  public toJson() {
    return {
      message: this.message,
      debugger: this.stack,
      errorCode: this.errorCode,
      statusCode: this.statusCode
    }
  }
}

export default BaseException
