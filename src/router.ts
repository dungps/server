import express, { Router } from "express"

type Method = "get" | "post" | "put" | "patch" | "delete" | "all"

class AppRouter {
  private router: express.Router

  constructor() {
    this.router = Router()
  }

  public getRouter(): express.Router {
    return this.router
  }

  public any(path: string, callback: express.RequestHandler | express.RequestHandler[]): AppRouter {
    this.router.all(path, callback)
    return this
  }

  public get(path: string, callback: express.RequestHandler | express.RequestHandler[]): AppRouter {
    this.router.get(path, callback)
    return this
  }

  public post(path: string, callback: express.RequestHandler | express.RequestHandler[]): AppRouter {
    this.router.post(path, callback)
    return this
  }

  public put(path: string, callback: express.RequestHandler | express.RequestHandler[]): AppRouter {
    this.router.put(path, callback)
    return this
  }

  public patch(path: string, callback: express.RequestHandler | express.RequestHandler[]): AppRouter {
    this.router.patch(path, callback)
    return this
  }

  public delete(path: string, callback: express.RequestHandler | express.RequestHandler[]): AppRouter {
    this.router.get(path, callback)
    return this
  }

  public redirect(from: string, to: string, status: 301 | 302 = 301): AppRouter {
    this.get(from, (_req: express.Request, res: express.Response) => {
      return res.redirect(to, status)
    })
    return this
  }

  public match(
    methods: Method[],
    path: string,
    callback: express.RequestHandler | express.RequestHandler[]
  ): AppRouter {
    methods.forEach((method: Method) => {
      if (this.router[method]) {
        this.router[method](path, callback)
      }
    })
    return this
  }
}

export default AppRouter
