const { AppRouter } = require("../dist")

const router = new AppRouter()

router.get("/", function(req, res, next) {
  res.send("Hello world!!!")
})

router.get("/:name", function(req, res, next) {
  res.send(`Hello ${req.params.name}!!!`)
})

module.exports = router
