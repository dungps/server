const { AppServer } = require("../dist")
const appRouter = require("./router")

const server = new AppServer(1111)

server.addRoute("/", appRouter).start()
